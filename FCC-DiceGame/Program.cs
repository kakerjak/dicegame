﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCC_DiceGame
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new DiceGame(5, 10000);
            Console.ReadLine();
        }
    }

    public class DiceGame
    {
        private Random rnd;
        private int numberOfDice;
        private int numberOfSimulations;

        public int GetNumberOfSimulations()
        {
            return numberOfSimulations;
        }

        public void SetNumberOfSimulations(int value)
        {
            numberOfSimulations = value;
        }

        public int GetNumberOfDice()
        {
            return numberOfDice;
        }

        public void SetNumberOfDice(int value)
        {
            numberOfDice = value;
        }
        
        public DiceGame(int NumberOfDice, int NumberOfSimulations)
        {
            rnd = new Random();
            SetNumberOfDice(NumberOfDice);
            SetNumberOfSimulations(NumberOfSimulations);
            if (numberOfDice > 0)
            {
                var logs = new List<GameLog>();
                for (var x = 0; x < numberOfSimulations; x++)
                {
                    logs.Add(RunGame());
                }
                PrintResults(logs);
            }
        }

        public GameLog RunGame()
        {
            var gameLog = new GameLog { };
            var diceInPlay = numberOfDice;

            do
            {
                var roll = DoRoll(diceInPlay);
                var c = roll.Count(r => r == 3);
                if (c > 0)
                {
                    gameLog.Rolls.Add(0);
                    diceInPlay = diceInPlay - c;
                }
                else
                {
                    gameLog.Rolls.Add(roll.Min());
                    diceInPlay--;
                }
            } while (diceInPlay > 0);

            return gameLog;
        }

        public void PrintResults(List<GameLog> logs)
        {
            Console.WriteLine($"Number of simulations was {numberOfSimulations} using {numberOfDice} dice, with a total score of all games of {logs.Sum(l => l.TotalScore)}.");


        }

        private int[] DoRoll(int rollSize)
        {
            var roll = new int[rollSize];

            for (var i = 0; i < rollSize; i++)
            {
                roll[i] = rnd.Next(1, 6);
            }

            return roll;
        }
    }

    public class GameLog
    {
        public int TotalScore => Rolls.Sum();
        public List<int> Rolls { get; set; }
        public GameLog()
        {
            Rolls = new List<int>();
        }
    } 
}
